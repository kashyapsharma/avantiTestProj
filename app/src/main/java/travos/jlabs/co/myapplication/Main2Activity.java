package travos.jlabs.co.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {

    private TabLayout tabs;
    TextView from;
    private ViewPager viewpager;
    ViewPagerAdapter adapter;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
    Context context;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabbed);
        context=this;
        initView();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(context, Create.class);
                intent.putExtra("pos",""+tabs.getSelectedTabPosition());
                startActivity(intent);
            }
        });
    }

    private void initView() {
        from=(TextView)findViewById(R.id.from);
        from.setText(getIntent().getStringExtra("from"));
        tabs = (TabLayout) findViewById(R.id.tabs);
        back= (ImageView)findViewById(R.id.back);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        /*Bundle bundle=new Bundle();
        bundle.putString("from",getIntent().getStringExtra("from"));
        FP fo=new FP();
        fo.setArguments(bundle);*/
        setupViewPager(viewpager);
        tabs.setupWithViewPager(viewpager);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                finish();
            }
        });
    }
    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(FP.newInstance("Physics"), "Physics");
        adapter.addFragment(FP.newInstance("Chemistry"), "Chemistry");
        adapter.addFragment(FP.newInstance("Maths"), "Maths");
        viewPager.setAdapter(adapter);
    }




    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }



        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }




}

