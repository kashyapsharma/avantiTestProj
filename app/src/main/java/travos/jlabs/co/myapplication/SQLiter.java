package travos.jlabs.co.myapplication;

        import android.content.ContentValues;
        import android.content.Context;
        import android.database.Cursor;
        import android.database.DatabaseUtils;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;
        import android.util.Log;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;

/**
 * Created by kashy on 06/20/17.
 */

public class SQLiter extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "Study";
    Context context;

    public SQLiter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_STUDY_TABLE = "CREATE TABLE Study ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "subject TEXT, " +
                "chapter TEXT, " +
                "content TEXT);";


        db.execSQL(CREATE_STUDY_TABLE);
        String CREATE_TITLE_TABLE = "CREATE TABLE Title ( " +
                "tid INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT UNIQUE );";


        db.execSQL(CREATE_TITLE_TABLE);

        String CREATE_Relation_TABLE = "CREATE TABLE Relation ( " +
                "rid INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "study_id INTEGER, " +
                "title_id INTEGER );";


        db.execSQL(CREATE_Relation_TABLE);
    }


    private static final String TABLE_STUDY = "Study";
    private static final String TABLE_TITLE = "Title";
    private static final String TABLE_RELATION = "Relation";
    private static final String KEY_ID = "id";
    private static final String KEY_SUBJECT = "subject";
    private static final String KEY_CHAPTER = "chapter";
    private static final String KEY_CONTENT = "content";
    private static final String KEY_TITLE = "title";
    private static final String KEY_STUDY_ID = "study_id";
    private static final String KEY_TITLE_ID = "title_id";


    public void addToTable(JSONObject tp){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_SUBJECT, tp.getString("subject"));
            values.put(KEY_CHAPTER, tp.getString("chapter"));
            values.put(KEY_CONTENT,  tp.getString("content"));
            db.insert(TABLE_STUDY, null, values);
            values.clear();
            db.close();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public long addToTitleTable(String title){

        long id =checkIfTitleExists(title);
        if(id>-1)
            return id;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE,  title);
        id = db.insert(TABLE_TITLE, null, values);
        values.clear();
        db.close();
        return id;
    }


    public long checkIfTitleExists(String title) {
        String query = "SELECT DISTINCT tid FROM " + TABLE_TITLE + " WHERE "+KEY_TITLE+" = '"+title+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        while(db.inTransaction())
        {

        }
        db.beginTransaction();
        Cursor cursor = db.rawQuery(query, null);
        db.endTransaction();
        long id=-1;
        if (cursor.moveToFirst()) {
            id=Integer.parseInt(cursor.getString(0));
        }
        db.close();
        return id;
    }


    public void removeFromTable(String KEY_ID)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_STUDY, KEY_ID + " = ?", new String[]{String.valueOf(KEY_ID)});
        db.close();
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.onCreate(db);
    }


    public ArrayList<ClassStudy> getAllStudy() {
        ArrayList<ClassStudy> Studied = new ArrayList<>();
        String query = "SELECT  * FROM " + TABLE_STUDY + "";
        SQLiteDatabase db = this.getWritableDatabase();
        while(db.inTransaction())
        {

        }
        db.beginTransaction();
        Cursor cursor = db.rawQuery(query, null);
        db.endTransaction();
        ClassStudy tp = null;
        if (cursor.moveToFirst()) {
            do {
                tp = new ClassStudy();
                tp.id=(Integer.parseInt(cursor.getString(0)));
                tp.subject =cursor.getString(1);
                tp.chapter =cursor.getString(2);
                tp.content=cursor.getString(3);

                Studied.add(tp);
            } while (cursor.moveToNext());
        }
        db.close();
        return Studied;
    }

    public ArrayList<ClassStudy> getAllTitles() {
        ArrayList<ClassStudy> Studied = new ArrayList<>();
        String query = "SELECT DISTINCT title FROM " + TABLE_TITLE + "";
        SQLiteDatabase db = this.getWritableDatabase();
        while(db.inTransaction())
        {

        }
        db.beginTransaction();
        Cursor cursor = db.rawQuery(query, null);
        db.endTransaction();
        ClassStudy tp = null;
        if (cursor.moveToFirst()) {
            do {
                tp = new ClassStudy();
//                tp.id=(Integer.parseInt(cursor.getString(0)));
//                tp.subject =cursor.getString(1);
//                tp.chapter =cursor.getString(2);
//                tp.content=cursor.getString(3);
                tp.title=cursor.getString(0);
                Studied.add(tp);
            } while (cursor.moveToNext());
        }
        db.close();
        return Studied;
    }
    public ArrayList<ClassStudy> getTitleID(String title) {
        ArrayList<ClassStudy> Studied = new ArrayList<>();
        String query = "SELECT DISTINCT tid FROM " + TABLE_TITLE + " WHERE "+KEY_TITLE+" = '"+title+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        while(db.inTransaction())
        {

        }
        db.beginTransaction();
        Cursor cursor = db.rawQuery(query, null);
        db.endTransaction();
        ClassStudy tp = null;
        if (cursor.moveToFirst()) {
            do {
                tp = new ClassStudy();
//                tp.id=(Integer.parseInt(cursor.getString(0)));
//                tp.subject =cursor.getString(1);
//                tp.chapter =cursor.getString(2);
//                tp.content=cursor.getString(3);
                tp.tid=Integer.parseInt(cursor.getString(0));
                Studied.add(tp);
            } while (cursor.moveToNext());
        }
        db.close();
        return Studied;
    }
    public ArrayList<ClassStudy> getSudyID(int tid) {
        ArrayList<ClassStudy> Studied = new ArrayList<>();
        String query = "SELECT DISTINCT study_id FROM " + TABLE_RELATION + " WHERE "+KEY_TITLE_ID+" = "+tid+"";
        SQLiteDatabase db = this.getWritableDatabase();
        while(db.inTransaction())
        {

        }
        db.beginTransaction();
        Cursor cursor = db.rawQuery(query, null);
        db.endTransaction();
        ClassStudy tp = null;
        if (cursor.moveToFirst()) {
            do {
                tp = new ClassStudy();
//                tp.id=(Integer.parseInt(cursor.getString(0)));
//                tp.subject =cursor.getString(1);
//                tp.chapter =cursor.getString(2);
//                tp.content=cursor.getString(3);
                tp.id=Integer.parseInt(cursor.getString(0));
                Studied.add(tp);
            } while (cursor.moveToNext());
        }
        db.close();
        return Studied;
    }


    public ArrayList<ClassStudy> getAllSubject(String subject) {
        ArrayList<ClassStudy> Studied = new ArrayList<>();
        String query = "SELECT  * FROM " + TABLE_STUDY + " where subject = '"+subject +"' ORDER BY id desc";
        Log.e("somee",query);
        SQLiteDatabase db = this.getWritableDatabase();
        while(db.inTransaction())
        {

        }
        db.beginTransaction();
        Cursor cursor = db.rawQuery(query, null);
        db.endTransaction();
        ClassStudy tp = null;
        if (cursor.moveToFirst()) {
            do {
                tp = new ClassStudy();
                tp.id=(Integer.parseInt(cursor.getString(0)));
                tp.subject =cursor.getString(1);
                tp.chapter =cursor.getString(2);
                tp.content=cursor.getString(3);

                Studied.add(tp);
            } while (cursor.moveToNext());
        }
        db.close();
        return Studied;
    }



    public ArrayList<ClassStudy> getAllStudyD(int subject) {
        ArrayList<ClassStudy> Studied = new ArrayList<>();
        String query = "SELECT  * FROM " + TABLE_STUDY + " where id = "+subject +"";
        Log.e("someet",query);
        SQLiteDatabase db = this.getWritableDatabase();
        while(db.inTransaction())
        {

        }
        db.beginTransaction();
        Cursor cursor = db.rawQuery(query, null);
        db.endTransaction();
        ClassStudy tp = null;
        if (cursor.moveToFirst()) {
            do {
                tp = new ClassStudy();
                tp.id=(Integer.parseInt(cursor.getString(0)));
                tp.subject =cursor.getString(1);
                tp.chapter =cursor.getString(2);
                tp.content=cursor.getString(3);

                Studied.add(tp);
            } while (cursor.moveToNext());
        }
        db.close();
        return Studied;
    }

    public ArrayList<ClassStudy> getAllSubjectByID(int sid) {
        ArrayList<ClassStudy> Studied = new ArrayList<>();
//        String query = "SELECT " + TABLE_STUDY + ".* , "+ TABLE_RELATION +".study_id as reference FROM "+ TABLE_RELATION +" LEFT JOIN "+TABLE_STUDY+" ON "+ TABLE_STUDY +".id = "+TABLE_RELATION+".title_id WHERE "+ TABLE_RELATION+".title_id = "+sid;
        String query = "SELECT a.* FROM "+TABLE_RELATION+" b LEFT JOIN "+TABLE_STUDY+" a ON a."+KEY_ID+"=b."+KEY_STUDY_ID+" WHERE b."+KEY_TITLE_ID+"=?";
        // String query="select * from Relation";
        Log.e("someet",query);
        SQLiteDatabase db = this.getWritableDatabase();
        while(db.inTransaction())
        {

        }
        db.beginTransaction();
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(sid)});
        db.endTransaction();
        ClassStudy tp = null;
        if (cursor.moveToFirst()) {
            do {
                DatabaseUtils.dumpCursor(cursor);
                tp = new ClassStudy();
                Log.e("west",cursor.getString(0));

                tp.id=(Integer.parseInt(cursor.getString(0)));
                tp.subject =cursor.getString(1);
                tp.chapter =cursor.getString(2);
                tp.content=cursor.getString(3);

                Studied.add(tp);
            } while (cursor.moveToNext());
        }
        db.close();
        return Studied;
    }

    public void updateRelationship(long title_id,int study_id){
        if(checkIfRelationExists(title_id,study_id))
            return;
        //String query = "UPDATE "+TABLE_Notification+" set "+KEY_Seen+"='0' WHERE "+KEY_ID+">=0";
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_STUDY_ID,  study_id);
        values.put(KEY_TITLE_ID,  title_id);
        db.insert(TABLE_RELATION, null, values);
        values.clear();
        db.close();
    }

    public boolean checkIfRelationExists(long title_id,int study_id) {
        String query = "SELECT * FROM " + TABLE_RELATION + " WHERE "+KEY_STUDY_ID+" = "+study_id+" AND "+KEY_TITLE_ID + " = "+title_id;
        SQLiteDatabase db = this.getWritableDatabase();
        while(db.inTransaction())
        {

        }
        db.beginTransaction();
        Cursor cursor = db.rawQuery(query, null);
        db.endTransaction();
        if (cursor.moveToFirst()) {
            return true;
        }
        db.close();
        return false;
    }


}