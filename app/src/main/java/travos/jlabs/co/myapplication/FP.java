package travos.jlabs.co.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by kashy on 06/17/17.
 */

public class FP extends Fragment {
    RecyclerView recycler;
    SQLiter sqliter;
    RecyclerView.LayoutManager layoutManager1;
    ArrayList<ClassStudy> items;
    public static final String ARG_SECTION_NUMBER = "section_number";

    public FP() {
        // Required empty public constructor
    }

    public static FP newInstance(String sectionNumber) {
        FP fragment = new FP();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_tabbed, container, false);
        Log.e("now m at", "brouse");
        sqliter = new SQLiter(getContext());
        recycler=(RecyclerView)rootView.findViewById(R.id.recycler);
        layoutManager1 = new GridLayoutManager(getContext(),1);
        Bundle bundle=getArguments();

        items = sqliter.getAllSubject(bundle.getString(ARG_SECTION_NUMBER));

        recycler.setAdapter(new RecyclerViewAdapter(getContext(),1,items));
        recycler.setLayoutManager(layoutManager1);
        return rootView;
    }
    private  class RecyclerViewAdapter extends RecyclerView.Adapter<FakeViewHolder> {

        Context context;
        int[] drawables;
        int[] text;
        int[] notif_count;
        JSONObject[] jsonObjects;

        ArrayList<ClassStudy> items;



        public RecyclerViewAdapter(Context context,int index,ArrayList<ClassStudy> items ) {
            this.items=items;
            this.context=context;
            if (index==1) {

            }

        }

        @Override
        public FakeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FakeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter, parent, false));
        }

        @Override
        public void onBindViewHolder(final FakeViewHolder holder, final int position) {

            holder.topic.setText(items.get(position).subject);
            holder.chapetr.setText(items.get(position).chapter);
            holder.content.setText(items.get(position).content);
            holder.parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent =new Intent (getContext(), FullPage.class);
                    intent.putExtra("id",items.get(position).id);
                    intent.putExtra("subject",items.get(position).subject);
                    intent.putExtra("chapter",items.get(position).chapter);
                    intent.putExtra("content",items.get(position).content);
                    startActivity(intent);
                }
            });


        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }
    private static class FakeViewHolder extends RecyclerView.ViewHolder {

        TextView topic,chapetr,content;
        RelativeLayout parent;


        public FakeViewHolder(View itemView) {
            super(itemView);
            topic = (TextView) itemView.findViewById(R.id.topic);
            chapetr = (TextView) itemView.findViewById(R.id.chapetr);
            content = (TextView) itemView.findViewById(R.id.content);
            parent = (RelativeLayout) itemView.findViewById(R.id.parent);


        }
    }
}