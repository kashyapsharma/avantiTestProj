package travos.jlabs.co.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import travos.jlabs.co.myapplication.AutoComplete.AutoComplTextView;

public class Create extends AppCompatActivity implements View.OnClickListener {
    SQLiter sqLiter;
    ArrayList<ClassStudy> itema;
    private TextView subject;
    private TextView date;
    private AutoComplTextView autochapter;
    private AppCompatEditText notes;
    private Button save;
    String so;
    ImageView back;
    List<String> chapters = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        sqLiter = new SQLiter(this);
        so=getIntent().getStringExtra("pos");
        Log.e("data",so);
        initView();
    }

    private void initView() {
        itema=sqLiter.getAllStudy();
        subject = (TextView) findViewById(R.id.subject);
        date = (TextView) findViewById(R.id.date);
        autochapter = (AutoComplTextView) findViewById(R.id.autochapter);
        notes = (AppCompatEditText) findViewById(R.id.notes);
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(this);
        if(so.equalsIgnoreCase("0")){
            subject.setText("Physics");
            chapters.add("Fundamental Concepts");
            chapters.add("Kinematics");
            chapters.add("Thermodynamics");
            chapters.add("Electricity and Magnetism");
            chapters.add("Oscillations and Waves");
            chapters.add("Atomic, Nuclear, and Particle Physics");
            chapters.add("Mechanics");
        }else if(so.equalsIgnoreCase("1")){
            subject.setText("Chemistry");
            chapters.add("Gases");
            chapters.add("Ionic Compounds");
            chapters.add("Redox Reactions");
            chapters.add("Transition Metal Chemistry");
            chapters.add("Organic Chemistry");
            chapters.add("Inorganic Chemistry");
            chapters.add("Biochemistry");
        } else{
            subject.setText("Maths");
            chapters.add("Algebra");
            chapters.add("Calculus");
            chapters.add("Geometry");
            chapters.add("Logic");
            chapters.add("Number Theory");
            chapters.add("Dynamical systems and differential equations");
            chapters.add("Mathematical Physics");
        }



        autochapter.setDatas(chapters);
        back= (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                finish();
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                submit();
                break;
        }
    }

    private void submit() {
        // validate
        String chapterString = autochapter.getText().toString().trim();
        if (TextUtils.isEmpty(chapterString)) {
            Toast.makeText(this, "Enter chapter name", Toast.LENGTH_SHORT).show();
            return;
        }

        String notesString = notes.getText().toString().trim();
        if (TextUtils.isEmpty(notesString)) {
            Toast.makeText(this, "Start writing here...", Toast.LENGTH_SHORT).show();
            return;
        }

        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("subject",subject.getText().toString().trim());
            jsonObject.put("chapter",chapterString);
            jsonObject.put("content",notesString);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sqLiter.addToTable(jsonObject);
        Intent intent =new Intent(this, Main2Activity.class);
        intent.putExtra("from","Study");
        startActivity(intent);


    }
}
