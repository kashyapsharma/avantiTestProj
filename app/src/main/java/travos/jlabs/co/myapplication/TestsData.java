package travos.jlabs.co.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

public class TestsData extends AppCompatActivity {

    private RecyclerView recycler;
    Context context;
    SQLiter sqliter;
    RecyclerView.LayoutManager layoutManager1;
    ArrayList<ClassStudy> itemsqa;
    ArrayList<ClassStudy> itemqs;
    ArrayList<ClassStudy> item;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_titles);
        context=this;
        initView();
    }

    private void initView() {


        recycler = (RecyclerView) findViewById(R.id.recycler);
        layoutManager1 = new GridLayoutManager(context,1);
        sqliter = new SQLiter(context);
        itemsqa = sqliter.getTitleID(getIntent().getStringExtra("from"));
        //itemqs=sqliter.getSudyID(itemsqa.get(0).tid);
        try {
            item=sqliter.getAllSubjectByID(itemsqa.get(0).tid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*String l="'";
        for (int i=0;i<itemqs.size();i++){
            l=l+itemqs.get(i).id+"','";
//            if (itemqs.size()==i){
//                l=l+"'";
//            }

        }
        Log.e("LOgg",l);
        item=sqliter.getAllStudyD(itemqs.get(0).id);*/

        recycler.setAdapter(new RecyclerViewAdapter(context,1,item));
        recycler.setLayoutManager(layoutManager1);
        back= (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                finish();
            }
        });
    }

    private  class RecyclerViewAdapter extends RecyclerView.Adapter<FakeViewHolder> {

        Context context;
        int[] drawables;
        int[] text;
        int[] notif_count;
        JSONObject[] jsonObjects;

        ArrayList<ClassStudy> items;



        public RecyclerViewAdapter(Context context,int index,ArrayList<ClassStudy> items ) {
            this.items=items;
            this.context=context;
            if (index==1) {

            }

        }

        @Override
        public FakeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FakeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_title, parent, false));
        }

        @Override
        public void onBindViewHolder(final FakeViewHolder holder, final int position) {

            holder.title.setText(item.get(position).subject);

            holder.parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent =new Intent (context, Main2Activity.class);

                    startActivity(intent);
                }
            });


        }

        @Override
        public int getItemCount() {
            return item.size();
        }
    }
    private static class FakeViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        RelativeLayout parent;


        public FakeViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            parent = (RelativeLayout) itemView.findViewById(R.id.parent);


        }
    }
}
