package travos.jlabs.co.myapplication.cust_compo;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import travos.jlabs.co.myapplication.R;

/**
 * Created by kashy on 06/22/17.
 */

public class FontText extends TextView {


    public FontText(Context context) {
        super(context);
        if(isInEditMode())
        {
            return;
        }
        Typeface tf = FontCache.get("fonts/Flaticon.ttf", context);
        if(tf != null) {
            this.setTypeface(tf);
        }
    }


    public FontText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface tf;
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.textfontstyle, 0, 0);
        String font_name = a.getString(R.styleable.textfontstyle_fontname);
        a.recycle();
        if(font_name==null)
        {
            font_name="ll";
        }
        if(font_name.equals("ll"))
        {
            tf= FontCache.get("fonts/Flaticon.ttf", context);
            setShadowLayer(2, 1, 1, Color.BLACK);
        }

        else
        {
            tf= FontCache.get("fonts/Flaticon.ttf", context);
            setShadowLayer(2, 1, 1, Color.BLACK);
        }

        if(tf != null) {
            this.setTypeface(tf);
        }
    }



    public FontText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface tf = FontCache.get("fonts/Flaticon.ttf", context);
        if(tf != null) {
            this.setTypeface(tf);

        }
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }


}
