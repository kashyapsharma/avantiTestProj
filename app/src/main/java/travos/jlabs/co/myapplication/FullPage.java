package travos.jlabs.co.myapplication;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import travos.jlabs.co.myapplication.AutoComplete.AutoComplTextView;


public class FullPage extends AppCompatActivity {

    private TextView chapter,subject,content;
    Button addtotitle;
    Intent intent;
    RelativeLayout bartitle;
    RelativeLayout relative;
    AutoComplTextView titles;
    ArrayList<ClassStudy> itema;
    SQLiter sqLiter;
    Button dialogButton;
    ImageView back;
    List<String> chapters = new ArrayList<String>();
    ArrayList<ClassStudy> tids;
    int sub_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_page);
        intent = getIntent();
        sqLiter = new SQLiter(this);
        itema=sqLiter.getAllTitles();

        initView();
    }

    private void initView() {
        dialogButton = (Button) findViewById(R.id.done);
        chapter = (TextView) findViewById(R.id.chapter);
        subject = (TextView) findViewById(R.id.subject);
        content = (TextView) findViewById(R.id.content);
        addtotitle = (Button) findViewById(R.id.addtotitle);
        bartitle = (RelativeLayout) findViewById(R.id.bartitle);
        relative = (RelativeLayout) findViewById(R.id.relative);
        titles = (AutoComplTextView) findViewById(R.id.title);
        sub_id=intent.getIntExtra("id",-1);
        chapter.setText(intent.getStringExtra("chapter"));
        subject.setText(intent.getStringExtra("subject"));
        content.setText(intent.getStringExtra("content"));




        Log.e("oppp",""+itema.size());
        for (int i=0;i<itema.size();i++){
            try {
                chapters.add(itema.get(i).title);
                Log.e("oppp",""+itema.get(i).title);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        chapters.add("Note 1");
        chapters.add("Phase 1");
        chapters.add("Exam 1");
        chapters.add("Test 1");
        // set the custom dialog components - text, image and button

        titles.setDatas(chapters);
        back= (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                finish();
            }
        });

        relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bartitle.setVisibility(View.GONE);
                addtotitle.setVisibility(View.VISIBLE);
            }
        });
        addtotitle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                bartitle.setVisibility(View.VISIBLE);
                addtotitle.setVisibility(View.GONE);

            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long id = sqLiter.addToTitleTable(titles.getText().toString().trim());
                //tids=sqLiter.getTitleID(titles.getText().toString().trim());
                sqLiter.updateRelationship(id,sub_id);
                titles.setText("");
                itema=sqLiter.getAllTitles();
                bartitle.setVisibility(View.GONE);
                addtotitle.setVisibility(View.VISIBLE);
            }
        });
        // TO DO AutoComplete for Title
    }
}

