package travos.jlabs.co.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    LinearLayout gen,tests;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        gen = (LinearLayout) findViewById(R.id.gen);
        tests = (LinearLayout) findViewById(R.id.tests);


        gen.setOnClickListener(this);
        tests.setOnClickListener(this);
        

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gen:
                Intent intent =new Intent (this, Main2Activity.class);
                intent.putExtra("from","Study");
                startActivity(intent);
                break;
            case R.id.tests:
                Intent intents =new Intent (this, Titles.class);
                startActivity(intents);
                break;

        }
    }
}
